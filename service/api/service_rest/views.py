from os import walk
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import  require_http_methods
import json
from django.db import IntegrityError 

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment

# Create your views here.
#

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties=[
        "href",
        "vin",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties= [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "is_vip"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        return {"is_vip": o.is_vip()}

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method=="GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians},
                            encoder=TechnicianEncoder)

    if request.method=="POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
        except  IntegrityError as e:
            print(str(e))
            return JsonResponse({"message": str(e)}, status=400)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)

@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, id):
    if request.method=="DELETE":
        try:
            count, _ = Technician.objects.get(id=id).delete()
        except Technician.DoesNotExist:
            return JsonResponse({"message": f"Technician with id {id} not found."}, status=404)
        return JsonResponse({"deleted": count >  0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method=="GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            { "appointments": appointments},
            encoder=AppointmentEncoder)
    else:
        content = json.loads(request.body)
        print(content)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid Technician ID"}, status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, id):
    if request.method=="DELETE":
        try:
            count, _ = Appointment.objects.get(id=id).delete()
        except Appointment.DoesNotExist:
            return JsonResponse({"message": f"Appointment with id {id} not found."}, status=404)
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": f"Appointment with id {id} not found."}, status=400)
    appointment.status = Appointment.Status.FINISHED
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse({"message": f"Appointment with id {id} not found."}, status=400)
    appointment.status = Appointment.Status.CANCELLED
    appointment.save()
    return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

def api_list_appointment_statuses(request):
    statuses = Appointment.Status.choices
    status_list = [{'value': value, 'label': label} for value, label in statuses]
    return JsonResponse(status_list, safe=False)
