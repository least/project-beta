from django.db import models
from django.template.defaultfilters import default

class AutomobileVO(models.Model):
    href = models.CharField(max_length=255, unique=True)
    vin = models.CharField(max_length=255, unique=True)
    sold= models.BooleanField(default=False)

class Technician(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    employee_id = models.IntegerField(unique=True)

class Appointment(models.Model):
    class Status(models.TextChoices):
        PENDING = "Pending", ("Pending")
        CANCELLED = "Cancelled", ("Cancelled")
        FINISHED = "Finished", ("Finished")

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=255)
    status = models.CharField(max_length=255, choices=Status.choices, default=Status.PENDING)
    vin = models.CharField(max_length=255)
    customer = models.CharField(max_length=255)

    def is_vip(self):
        sold_autos = AutomobileVO.objects.filter(vin=self.vin, sold=True)
        if len(sold_autos) > 0:
            return True
        return False


    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT)
