
from django.urls import path, include
from .views import api_cancel_appointment, api_finish_appointment, api_list_appointment_statuses, api_list_appointments, api_list_technicians, api_show_appointment, api_show_technician


urlpatterns = [
    path('technicians/', api_list_technicians, name="api_list_technicians"),
    path('technicians/<int:id>/', api_show_technician, name="api_show_technician"),
    path('appointments/', api_list_appointments, name="api_list_appointments"),
    path('appointments/statuses/', api_list_appointment_statuses, name="statuses"),
    path('appointments/<int:id>/', api_show_appointment, name="api_show_appointment"),
    path('appointments/<int:id>/finish/', api_finish_appointment, name="api_finish_appointment"),
    path('appointments/<int:id>/cancel/', api_cancel_appointment, name="api_cancel_appointment"),
]
