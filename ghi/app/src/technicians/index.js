import { useState, useEffect } from "react";
export default function Technicians() {
  const [technicians, setTechnicians] = useState([]);

  const fetchTechnicians = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const res = await fetch(url);
    if (res.ok) {
      const { technicians } = await res.json();
      setTechnicians(technicians);
    }
  };
  useEffect(() => {
    fetchTechnicians();
  }, []);
  return (
    <>
      <h2 className="mt-4">Technicians</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((tech) => {
            return (
              <tr key={tech.employee_id}>
                <td>{tech.employee_id}</td>
                <td>{tech.first_name}</td>
                <td>{tech.last_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
