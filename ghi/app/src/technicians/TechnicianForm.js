import { useState, useEffect } from "react";
import Message from "../Components/Message";
export default function TechnicianForm({ statusType, fetchTechnicians }) {
  const [formData, setFormData] = useState({});
  const [submitStatus, setSubmitStatus] = useState(statusType.PENDING);
  const [message, setMessage] = useState("");
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const createTechnicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const res = await fetch(createTechnicianUrl, fetchConfig);
      if (res.ok) {
        const technician = await res.json();
        setFormData({});
        setSubmitStatus(statusType.SUCCESS);
        setMessage(
          `${res.status}: Successfully Added Technician, ${technician.first_name} ${technician.last_name}`
        );

        fetchTechnicians();
      } else {
        setSubmitStatus(statusType.FAILURE);
        setMessage(res.status + ": " + res.statusText);
      }
    } catch (e) {
      // console.error(e);
    }
  };

  return (
    <>
      <div className="row">
        <div className="col-sm-10, offset-sm-1 offset-md-3 col-md-6">
          <div className="shadow mt-4 p-4">
            <h1 className="mb-4">Add Technician</h1>
            {submitStatus === statusType.FAILURE && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            {submitStatus === statusType.SUCCESS && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="first_name"
                  placeholder="First Name"
                  value={formData.first_name ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="last_name"
                  placeholder="Last Name"
                  value={formData.last_name ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="number"
                  name="employee_id"
                  placeholder="Employee ID"
                  value={formData.employee_id ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
