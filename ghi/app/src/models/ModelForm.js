import { useEffect, useState } from "react";
import Message from "../Components/Message";
export default function ModelForm({ fetchModels, statusType, manufacturers }) {
  const [formData, setFormData] = useState({});
  const [submitStatus, setSubmitStatus] = useState(statusType.PENDING);
  const [message, setMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const url = "http://localhost:8100/api/models/";
    const res = await fetch(url, {
      method: "post",
      body: JSON.stringify(formData),
    });
    if (res.ok) {
      const data = await res.json();
      setFormData({});
      setSubmitStatus(statusType.SUCCESS);
      setMessage(`Successfully Added Model, ${formData.name}`);
      fetchModels();
    } else {
      setSubmitStatus(statusType.FAILURE);
      setMessage(res.status + ": " + res.statusText);
    }
  };

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  return (
    <>
      <div className="row">
        <div className="col-sm-10, offset-sm-1 offset-md-3 col-md-6">
          <div className="shadow mt-4 p-4">
            <h1 className="mb-4">Add Model</h1>
            {submitStatus === statusType.FAILURE && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            {submitStatus === statusType.SUCCESS && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="name"
                  placeholder="Name"
                  value={formData.name ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="picture_url"
                  placeholder="Picture URL"
                  value={formData.picture_url ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="form-floating mb-3">
                <select
                  name="manufacturer_id"
                  id="manufacturer-select"
                  className="form-select"
                  value={formData.manufacturer ?? ""}
                  onChange={handleChange}
                >
                  <option defaultValue value="">
                    Choose a Manufacturer
                  </option>
                  {manufacturers.map((manufacturer) => {
                    return (
                      <option
                        key={manufacturer.name}
                        name="manufacturer_id"
                        value={manufacturer.id}
                      >
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
                <label htmlFor="manufacturer_id">Manufacturer</label>
              </div>
              <button className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
