export default function Models({ models }) {
  return (
    <>
      <h2 className="mt-4">Models</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.name}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img
                    style={{ maxWidth: "300px", height: "auto" }}
                    src={model.picture_url}
                    alt={model.name}
                  />{" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
