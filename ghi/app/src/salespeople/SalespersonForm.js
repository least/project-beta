import React, { useState } from 'react';

function SalespersonForm({fetchSalespeople}) {
    const [first_name, setFirstName] = useState([]);
    const [last_name, setLastName] = useState([]);
    const [employee_id, setEmployeeId] = useState([]);

    const handleFirstNameChange = event => {
        setFirstName(event.target.value);
    };
    const handleLastNameChange = event => {
        setLastName(event.target.value);
    };
    const handleEmployeeIdChange = event => {
        setEmployeeId(event.target.value);
    };

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {}
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            fetchSalespeople();
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        };
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className='shadow p-4 mt-4'>
                    <h1>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id="add-salesperson-form">
                        <div className="form-floating mb-3">
                            <label htmlFor="first_name"></label>
                            <input value={first_name} onChange={handleFirstNameChange} placeholder="First Name" type="text" id="first_name" />
                        </div>
                        <div className="form-floating mb-3">
                            <label htmlFor="last_name"></label>
                            <input value={last_name} onChange={handleLastNameChange} placeholder="Last Name" type="text" id="last_name" />
                        </div>
                        <div className="form-floating mb-3">
                            <label htmlFor="employee_id"></label>
                            <input value={employee_id} onChange={handleEmployeeIdChange} placeholder="Employee ID" type="text" id="employee_id" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SalespersonForm;
