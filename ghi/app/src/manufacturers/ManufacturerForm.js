import { useState } from "react";
import Message from "../Components/Message";
export default function ManufacturerForm({ fetchManufacturers, statusType }) {
  const [manufacturer, setManufacturer] = useState("");
  const [submitStatus, setSubmitStatus] = useState(statusType.PENDING);
  const [message, setMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const url = "http://localhost:8100/api/manufacturers/";
    const res = await fetch(url, {
      method: "post",
      body: JSON.stringify({ name: manufacturer }),
    });
    if (res.ok) {
      setManufacturer("");
      setSubmitStatus(statusType.SUCCESS);
      setMessage(`Successfully Added Manufacturer, ${manufacturer}`);
      fetchManufacturers();
    } else {
      setSubmitStatus(statusType.FAILURE);
      setMessage(res.status + ": " + res.statusText);
    }
  };

  const handleChange = (e) => {
    setManufacturer(e.target.value);
  };
  return (
    <>
      <div className="row">
        <div className="col-sm-10, offset-sm-1 offset-md-3 col-md-6">
          <div className="shadow mt-4 p-4">
            <h1 className="mb-4">Add Manufacturer</h1>
            {submitStatus === statusType.FAILURE && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            {submitStatus === statusType.SUCCESS && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="manufacturer"
                  placeholder="Manufacturer Name"
                  value={manufacturer}
                  onChange={handleChange}
                />
                <label htmlFor="manufacturer">Manufacturer Name</label>
              </div>
              <button className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
