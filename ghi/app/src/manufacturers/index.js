export default function Manufacturers({ manufacturers }) {
  return (
    <>
      <h2 className="mt-4">Manufacturers</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturers</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => {
            return (
              <tr key={manufacturer.name + manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
