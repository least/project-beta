import { useState, useEffect } from "react";
import Message from "../Components/Message";
export default function AppointmentForm({
  statusType,
  technicians,
  fetchAppointments,
}) {
  const [formData, setFormData] = useState({});
  const [submitStatus, setSubmitStatus] = useState(statusType.PENDING);
  const [message, setMessage] = useState("");
  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const res = await fetch(appointmentUrl, fetchConfig);
      if (res.ok) {
        const appointment = await res.json();
        setFormData({});
        setSubmitStatus(statusType.SUCCESS);
        setMessage(
          `Successfully Added Appointment for ${appointment.customer}`
        );
        fetchAppointments();
      } else {
        // console.error(res.status, res.statusText);
        setSubmitStatus(statusType.FAILURE);
        setMessage(res.status + ": " + res.statusText);
      }
    } catch (e) {
      // console.error(e);
    }
  };

  return (
    <>
      <div className="row">
        <div className="col-sm-10, offset-sm-1 offset-md-3 col-md-6">
          <div className="shadow mt-4 p-4">
            <h1 className="mb-4">Add Appointment</h1>
            {submitStatus === statusType.FAILURE && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            {submitStatus === statusType.SUCCESS && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="vin"
                  placeholder="VIN"
                  value={formData.vin ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="customer"
                  placeholder="Customer"
                  value={formData.customer ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="datetime-local"
                  name="date_time"
                  placeholder="Date Time"
                  value={formData.date_time ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="date_time">Date Time</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="reason"
                  placeholder="Reason"
                  value={formData.reason ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <select
                  name="technician"
                  id="technician-select"
                  className="form-select"
                  value={formData.technician ?? ""}
                  onChange={handleChange}
                >
                  <option defaultValue value="">
                    Choose a Technician
                  </option>
                  {technicians.map((technician) => {
                    return (
                      <option
                        key={technician.employee_id}
                        name="technician"
                        value={technician.id}
                      >
                        {technician.first_name} {technician.last_name}
                      </option>
                    );
                  })}
                </select>
                <label htmlFor="technician">Technician</label>
              </div>

              <button className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
