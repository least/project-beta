import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
export default function Appointments({
  appointments,
  fetchAppointments,
  historyView,
}) {
  const [vinSearch, setVinSearch] = useState("");
  const [filterArray, setFilterArray] = useState([]);
  const [searchParams, setSearchParams] = useSearchParams();

  useEffect(() => {
    if (searchParams.get("vin")) {
      setVinSearch(searchParams.get("vin"));
    }
  }, [searchParams]);

  useEffect(() => {
    if (historyView) {
      setFilterArray(["Cancelled", "Finished"]);
    } else {
      setFilterArray(["Pending"]);
    }
  }, [historyView]);

  const handleChange = (e) => {
    setVinSearch(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setSearchParams({ vin: vinSearch });
  };

  const searchVin = (vin, searchTerm) => {
    const loweredVin = vin.toLowerCase();
    const loweredSearch = searchTerm.toLowerCase();
    if (!loweredSearch) {
      return true;
    }
    return loweredVin.includes(loweredSearch);
  };
  const handleCancel = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
    const res = await fetch(url, { method: "PUT" });
    if (res.ok) {
      fetchAppointments();
    }
  };
  const handleFinish = async (id) => {
    const url = `http://localhost:8080/api/appointments/${id}/finish/`;
    const res = await fetch(url, { method: "PUT" });
    if (res.ok) {
      fetchAppointments();
    }
  };
  return (
    <>
      <h2 className="mt-4">
        {historyView ? "Service History" : "Service Appointments"}
      </h2>
      <form onSubmit={handleSubmit}>
        <div className="input-group mb-3">
          <span className="input-group-text">Search VIN:</span>
          <input
            className="form-control"
            type="text"
            name="vin"
            placeholder="VIN"
            value={vinSearch ?? ""}
            onChange={handleChange}
          />
          <label className="visually-hidden" htmlFor="vin">
            VIN
          </label>
          <button className="btn btn-secondary">Search</button>
        </div>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">VIN</th>
            <th scope="col">Is VIP?</th>
            <th scope="col">Customer</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            {historyView && <th scope="col">Status</th>}
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appt) => filterArray.includes(appt.status))
            .filter((appt) => searchVin(appt.vin, vinSearch))
            .map((appt) => {
              return (
                <tr key={appt.customer}>
                  <td>{appt.vin}</td>
                  <td>{appt.is_vip ? "Yes" : "No"}</td>
                  <td>{appt.customer}</td>
                  <td>{new Date(appt.date_time).toLocaleDateString()}</td>
                  <td>{new Date(appt.date_time).toLocaleTimeString()}</td>
                  <td>
                    {appt.technician.first_name} {appt.technician.last_name}
                  </td>
                  <td>{appt.reason}</td>
                  {historyView && <td>{appt.status}</td>}
                  {!historyView && (
                    <>
                      <td>
                        <button
                          onClick={() => handleCancel(appt.id)}
                          className="btn btn-danger"
                        >
                          Cancel
                        </button>
                      </td>
                      <td>
                        <button
                          onClick={() => handleFinish(appt.id)}
                          className="btn btn-success"
                        >
                          Finish
                        </button>
                      </td>
                    </>
                  )}
                </tr>
              );
            })}
        </tbody>
      </table>
    </>
  );
}
