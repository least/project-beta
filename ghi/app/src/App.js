import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useState, useEffect } from "react";
import MainPage from "./MainPage";
import Nav from "./Nav";
import Technicians from "./technicians";
import TechnicianForm from "./technicians/TechnicianForm";
import Appointments from "./appointments";
import AppointmentForm from "./appointments/AppointmentForm";
import Manufacturers from "./manufacturers";
import ManufacturerForm from "./manufacturers/ManufacturerForm";
import SalespersonForm from "./salespeople/SalespersonForm";
import SalespeopleList from "./salespeople/SalespeopleList";
import Models from "./models";
import ModelForm from "./models/ModelForm";
import CustomerForm from "./customers/CustomerForm";
import CustomersList from "./customers/CustomersList";
import SaleForm from "./sales/SaleForm";
import SalesList from "./sales/SalesList";
import SalespersonHistory from "./salespeople/SalespersonHistory";
import Automobiles from "./automobiles";
import AutomobileForm from "./automobiles/AutomobileForm";

function App() {
  const statusType = {
    PENDING: "Pending",
    SUCCESS: "Success",
    FAILURE: "Failure",
  };
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [models, setModels] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [sales, setSales] = useState([]);

  const fetchAppointments = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const res = await fetch(url);
    if (res.ok) {
      const { appointments } = await res.json();
      setAppointments(appointments);
    }
  };

  const fetchManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const res = await fetch(url);
    if (res.ok) {
      const { manufacturers } = await res.json();
      setManufacturers(manufacturers);
    }
  };
  const fetchTechnicians = async () => {
    const url = "http://localhost:8080/api/technicians/";
    const res = await fetch(url);
    if (res.ok) {
      const { technicians } = await res.json();
      setTechnicians(technicians);
    }
  };

  const fetchModels = async () => {
    const url = "http://localhost:8100/api/models/";
    const res = await fetch(url);
    if (res.ok) {
      const { models } = await res.json();
      setModels(models);
    }
  };
  const fetchSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  };
  const fetchCustomers = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };
  const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const res = await fetch(url);
    if (res.ok) {
      const { autos } = await res.json();
      setAutomobiles(autos);
    }
  };
  const fetchSales = async () => {
    const url = "http://localhost:8090/api/sales/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  };
  useEffect(() => {
    fetchTechnicians();
    fetchAppointments();
    fetchManufacturers();
    fetchModels();
    fetchSalespeople();
    fetchCustomers();
    fetchAutomobiles();
    fetchSales();
  }, []);
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/appointments/">
            <Route
              index
              element={
                <Appointments
                  appointments={appointments}
                  fetchAppointments={fetchAppointments}
                />
              }
            />
            <Route
              path="history"
              element={
                <Appointments appointments={appointments} historyView={true} />
              }
            />
            <Route
              path="create"
              element={
                <AppointmentForm
                  statusType={statusType}
                  technicians={technicians}
                  fetchAppointments={fetchAppointments}
                />
              }
            />
          </Route>
          <Route path="/technicians/">
            <Route
              index
              element={
                <Technicians
                  technicicians={technicians}
                  fetchTechnicians={fetchTechnicians}
                />
              }
            />
            <Route
              path="create"
              element={
                <TechnicianForm
                  fetchTechnicians={fetchTechnicians}
                  statusType={statusType}
                />
              }
            />
          </Route>
          <Route path="/manufacturers/">
            <Route
              index
              element={<Manufacturers manufacturers={manufacturers} />}
            />
            <Route
              path="create"
              element={
                <ManufacturerForm
                  fetchManufacturers={fetchManufacturers}
                  statusType={statusType}
                />
              }
            />
          </Route>
          <Route path="salespeople">
            <Route
              path="new"
              element={<SalespersonForm fetchSalespeople={fetchSalespeople} />}
            />
            <Route
              index
              element={<SalespeopleList salespeople={salespeople} />}
            />
          </Route>
          <Route path="customers">
            <Route
              path="new"
              element={<CustomerForm fetchCustomers={fetchCustomers} />}
            />
            <Route index element={<CustomersList customers={customers} />} />
          </Route>

          <Route path="/automobiles/">
            <Route index element={<Automobiles automobiles={automobiles} />} />
            <Route
              path="create"
              element={
                <AutomobileForm
                  statusType={statusType}
                  fetchAutomobiles={fetchAutomobiles}
                  models={models}
                />
              }
            />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList sales={sales} />} />
            <Route path="new" element={<SaleForm fetchAutomobiles={fetchAutomobiles} fetchSales={fetchSales}/>} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="/models/">
            <Route index element={<Models models={models} />} />
            <Route
              path="create"
              element={
                <ModelForm
                  manufacturers={manufacturers}
                  fetchModels={fetchModels}
                  statusType={statusType}
                />
              }
            />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
