import { useEffect, useState } from "react";
import Message from "../Components/Message";
export default function AutomobileForm({
  statusType,
  fetchAutomobiles,
  models,
}) {
  const [formData, setFormData] = useState({});
  const [submitStatus, setSubmitStatus] = useState(statusType.PENDING);
  const [message, setMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const res = await fetch(automobileUrl, fetchConfig);
      if (res.ok) {
        const auto = await res.json();
        fetchAutomobiles();
        setFormData({});
        setSubmitStatus(statusType.SUCCESS);
        setMessage(
          `Successfully added ${auto.model.manufacturer.name} ${auto.model.name} with VIN: ${auto.vin}`
        );
        fetchAutomobiles();
      } else {
        // console.error(res.status, res.statusText);
        setSubmitStatus(statusType.FAILURE);
        setMessage(res.status + ": " + res.statusText);
      }
    } catch (e) {
      // console.error(e);
    }
  };

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  return (
    <>
      <div className="row">
        <div className="col-sm-10, offset-sm-1 offset-md-3 col-md-6">
          <div className="shadow mt-4 p-4">
            <h1 className="mb-4">Add Automobile</h1>
            {submitStatus === statusType.FAILURE && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            {submitStatus === statusType.SUCCESS && (
              <Message
                message={message}
                statusType={statusType}
                submitStatus={submitStatus}
              />
            )}
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="vin"
                  placeholder="vin"
                  value={formData.vin ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="text"
                  name="color"
                  placeholder="color"
                  value={formData.color ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  className="form-control"
                  type="number"
                  name="year"
                  placeholder="year"
                  value={formData.year ?? ""}
                  onChange={handleChange}
                />
                <label htmlFor="year">Year</label>
              </div>

              <div className="form-floating mb-3">
                <select
                  name="model_id"
                  id="model-select"
                  className="form-select"
                  value={formData.model_id ?? ""}
                  onChange={handleChange}
                >
                  <option defaultValue value="">
                    Choose a Model
                  </option>
                  {models.map((model) => {
                    return (
                      <option key={model.name} name="model" value={model.id}>
                        {model.manufacturer.name} {model.name}
                      </option>
                    );
                  })}
                </select>
                <label htmlFor="model">model</label>
              </div>
              <button className="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
