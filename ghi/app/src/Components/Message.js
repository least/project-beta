export default function Message({ message, statusType, submitStatus }) {
  if (submitStatus === statusType.SUCCESS) {
    return (
      <div className="alert alert-success" role="alert">
        {message}
      </div>
    );
  }
  if (submitStatus === statusType.FAILURE) {
    return (
      <div className="alert alert-danger" role="alert">
        {message}
      </div>
    );
  }
}
