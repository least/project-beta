from django.contrib import admin
from .models import Salesperson, AutomobileVO, Sale

admin.site.register(Salesperson)
admin.site.register(AutomobileVO)
admin.site.register(Sale)
