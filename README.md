# CarCar

- [CarCar](#carcar)
  - [Getting Started](#getting-started)
  - [Design](#design)
  - [Service microservice](#service-microservice)
    - [Appointments](#appointments)
      - [Model](#model)
      - [Endpoints](#endpoints)
    - [Technicians](#technicians)
      - [Model](#model-1)
      - [Endpoints](#endpoints-1)
    - [AutomobileVO](#automobilevo)
      - [Model](#model-2)
  - [Sales microservice](#sales-microservice)
    - [Salesperson model](#salesperson-model)
      - [Endpoints](#endpoints-2)
      - [Examples](#examples)
    - [Customer model](#customer-model)
      - [Endpoints](#endpoints-3)
      - [Examples](#examples-1)
    - [Sale model](#sale-model)
      - [Endpoints](#endpoints-4)
      - [Examples](#examples-2)
    - [AutomobileVO model](#automobilevo-model)

Team:

- Libby Eilbert - Automobile Sales
- Joshua Vasquez (@least) - Automobile Service

## Getting Started

1. Fork this repository

2. Input the following into the terminal:

```bash
git clone forked.repo.url        # Clone the forked repo's URL
cd project-beta/                 # change directory into the project directory
docker volume create beta-data   # create a docker volume for the database
docker-compose build             # build the docker-compose.yml file
docker-compose up                # bring the containers up to run the application
```

The project frontend of the application will be located at http://localhost:3000

## Design

![Design Diagram](service-diagram.png)

## Service microservice

The "Service" microservice is responsible for managing service appointments and the dealership's technicians.

### Appointments

The appointment model is used to track and manage service appointments with the car dealership.

#### Model

| Name           | Data Type              | Explanation                                                                                                                                                   |
| -------------- | ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **customer**   | `CharField`            | The Customer's name                                                                                                                                           |
| **vin**        | `CharField`            | The VIN of the vehicle being serviced                                                                                                                         |
| **date_time**  | `DateTime`             | The date and time of the scheduled appointment                                                                                                                |
| **reason**     | `CharField`            | The reason for the vehicle's service appointment                                                                                                              |
| **status**     | `CharField`            | Tracks the status of the service. It can be "Pending", "Cancelled", or "Finished"                                                                             |
| **is_vip()**   | `function`             | Searches the `AutomobileVO` table and checks if appointment vehicle's **vin** matches the **vin** of a **sold** vehicle and marks the customer as a VIP if so |
| **technician** | `Foreign Relationship` | The technician assigned to the service appointment                                                                                                            |

#### Endpoints

| Action             | Method | URL                                                            |
| ------------------ | ------ | -------------------------------------------------------------- |
| List Appointments  | GET    | http://localhost:8080/api/appointments/                        |
| Create Appointment | POST   | http://localhost:8080/api/appointments/                        |
| Delete Appointment | DELETE | http://localhost:8080/api/appointments/:appointment.id/        |
| Cancel Appointment | PUT    | http://localhost:8080/api/appointments/:appointment.id/cancel/ |
| Finish Appointment | PUT    | http://localhost:8080/api/appointments/:appointment.id/finish/ |

<details>

<summary>

Examples

</summary>

```jsonc
// List Appointments: GET http://localhost:8080/api/appointments/
// Example Output - 200 OK
{
  "appointments": [
    {
      "id": 3,
      "date_time": "2023-07-25T00:28:34+00:00",
      "reason": "window shattered",
      "status": "Finished",
      "vin": "asldkfjaf183fsifj",
      "customer": "John Romero",
      "technician": {
        "id": 1,
        "first_name": "Jerry",
        "last_name": "Garcia",
        "employee_id": 1828389483
      },
      "is_vip": false
    },
    {
      "id": 6,
      "date_time": "2023-07-25T00:28:34+00:00",
      "reason": "Apple Stuck in Windshield",
      "status": "Cancelled",
      "vin": "asldkfjaf183fsifj",
      "customer": "Johnny ive",
      "technician": {
        "id": 16,
        "first_name": "Joe",
        "last_name": "Namath",
        "employee_id": 342342352
      },
      "is_vip": false
    }
  ]
}
```

```jsonc
// Create Appointment: POST http://localhost:8080/api/appointments/
// Example JSON body input
{
  "date_time": "2023-07-25T00:28:34+00:00",
  "reason": "oil change",
  "status": "Pending", // defaults to "Pending"
  "vin": "KL4CJFSB9FB300004",
  "customer": "Johnny Marr",
  "technician": 16
}
```

```jsonc
// Delete Appointment: DELETE http://localhost:8080/api/appointments/:appointment.id/
// Example output - 200 OK
{
	"deleted": true
}
// Example out - 404 Not Found
{
	"message": "Appointment with id 43 not found."
}
```

```jsonc
// Finish Appointment: PUT http://localhost:8080/api/appointments/:appointment.id/finish/
// Example Output - 200 OK
{
  "id": 9,
  "date_time": "2023-07-25T00:28:34+00:00",
  "reason": "needs oil change",
  "status": "Finished",
  "vin": "Aslkded",
  "customer": "Johnny Marr",
  "technician": {
    "id": 16,
    "first_name": "Joe",
    "last_name": "Namath",
    "employee_id": 342342352
  }
}
```

```jsonc
// Cancel Appointment: PUT http://localhost:8080/api/appointments/:appointment.id/cancel/
// Example Output - 200 OK
{
  "id": 6,
  "date_time": "2023-07-25T00:28:34+00:00",
  "reason": "Apple Stuck in Windshield",
  "status": "Cancelled",
  "vin": "asldkfjaf183fsifj",
  "customer": "Johnny ive",
  "technician": {
    "id": 16,
    "first_name": "Joe",
    "last_name": "Namath",
    "employee_id": 342342352
  },
  "is_vip": false
}
```

</details>

### Technicians

The Technician model is for managing technicians at the dealership and assign them to specific service appointments.

#### Model

| Name            | Data Type      | Explanation              |
| --------------- | -------------- | ------------------------ |
| **first_name**  | `CharField`    | Technician's first name  |
| **last_name**   | `CharField`    | Technician's last name   |
| **employee_id** | `IntegerField` | Technician's employee id |

#### Endpoints

| Action            | Method | URL                                                   |
| ----------------- | ------ | ----------------------------------------------------- |
| List Technicians  | GET    | http://localhost:8080/api/technicians/                |
| Add Technician    | POST   | http://localhost:8080/api/technicians/                |
| Delete Technician | DELETE | http://localhost:8080/api/technicians/:technician.id/ |

<details>

<summary>

Examples

</summary>

```jsonc
// List Technicians: POST http://localhost:8080/api/technicians/
// Example Output - 200 OK
{
  "technicians": [
    {
      "id": 1,
      "first_name": "Jerry",
      "last_name": "Garcia",
      "employee_id": 1828389483
    },
    {
      "id": 2,
      "first_name": "Jeremy",
      "last_name": "Bork",
      "employee_id": 178231298
    }
  ]
}
```

```jsonc
// Add Technician: POST http://localhost:8080/api/technicians/
// Example Input
{
  "first_name": "Joseph",
  "last_name": "Bryant",
  "employee_id": "12358893"
}
```

```jsonc
// Delete Technician: DELETE http://localhost:8080/api/technicians/:technician.id/
// Example Output - 200 Success
{
	"deleted": true
}
// Example Output - 404 Not Found
{
	"message": "Technician with id 13 not found."
}
```

</details>

### AutomobileVO

The AutomobileVO retrieves data from the Inventory service using polling. It is used to find VIP customers for service appointments. It is only used internally and thus does not have any API endpoints.

#### Model

| Name     | Data Type      | Explanation                                              |
| -------- | -------------- | -------------------------------------------------------- |
| **href** | `CharField`    | Path to the Automobile resource on the Inventory service |
| **vin**  | `CharField`    | The automobile's VIN number                              |
| **sold** | `BooleanField` | Boolean that indicates if a vehicle has been sold        |

## Sales microservice

The sales microservice tracks data about salespeople, customers, and the sales of automobiles. It has 4 models: Salesperson, Customer, Sale, and AutomobileVO. The AutomobileVO model is a value object that gets its data from the inventory microservice using a poller. It polls for instances of the Automobile model so that they can be referenced when recording a new sale.

### Salesperson model

The salesperson model tracks salespeople using their first name, last name, and employee id.

#### Endpoints

List salespeople: GET http://localhost:8090/api/salespeople/

Create salesperson: POST http://localhost:8090/api/salespeople/

See salesperson details by id (not employee_id): GET http://localhost:8090/api/salespeople/<salesperson.id>/

Delete a salesperson: DELETE http://localhost:8090/api/salespeople/<salesperson.id>/

#### Examples

To create a new salesperson:

```json
{
	"first_name": "Tara",
	"last_name": "George",
	"employee_id": 7777
}
```

return value:

```json
{
	"first_name": "Tara",
	"last_name": "George",
	"employee_id": 7777,
	"id": 11
}
```

return value of list salespeople:

```json
{
	"salespeople": [
		{
			"first_name": "Tara",
			"last_name": "George",
			"employee_id": 7777,
			"id": 1
		}
	]
}
```

return value of a successful delete:

```json
{
	"message": "Delete successful"
}
```

return value of trying to delete a nonexistent salesperson:

```json
{
	"error": "Salesperson does not exist"
}
```

### Customer model

The customer model tracks customers by the first name, last name, address, and phone number.

#### Endpoints

List customers: GET http://localhost:8090/api/customers/

Create a new customer: POST http://localhost:8090/api/customers/

View details of a customer by id: GET http://localhost:8090/api/customers/<customer.id>/

Delete a customer by id: http://localhost:8090/api/customers/<customer.id>/

#### Examples

To create a new customer:

```json
{
	"first_name": "Neil",
	"last_name": "Powers",
    "address": "123 45th Ave",
	"phone_number": "4085555555"
}
```

return value:

```json
{
	"first_name": "Neil",
	"last_name": "Powers",
    "address": "123 45th Ave",
	"phone_number": "4085555555",
	"id": 2
}
```

return value of listing customers:

```json
{
	"customers": [
		{
			"first_name": "Neil",
			"last_name": "Powers",    "address": "123 45th Ave",
			"phone_number": "4085555555",
			"id": 2
		}
	]
}
```

return value of a successful delete:

```json
{
	"message": "Delete successful"
}
```

return value of trying to delete a nonexistent customer:

```json
{
	"error": "Customer does not exist"
}
```

### Sale model

The sale model uses data from the Salesperson, Customer, and AutomobileVO models to track sales of automobiles, also including a price attribute.

#### Endpoints

List all sales: GET http://localhost:8090/api/sales/

List an individual salesperson's sales: GET http://localhost:8090/api/salespeople/<employee.id>/sales/

Create a new sale: POST http://localhost:8090/api/sales/

Delete a sale by id: DELETE http://localhost:8090/api/sales/<employee.id>/

#### Examples

to create a new sale, referencing the href of the automobile value object:

```json
{
	"automobile": "/api/automobiles/1C3CC5FB2AN120174/",
	"salesperson": 1,
	"customer": 2,
	"price": 22000.00
}
```

return value:

```json
{
	"automobile": {
		"import_href": "/api/automobiles/1C3CC5FB2AN120174/",
		"vin": "1C3CC5FB2AN120174",
		"sold": true
	},
	"salesperson": {
		"first_name": "Tara",
		"last_name": "George",
		"employee_id": 7777,
		"id": 1
	},
	"customer": {
		"first_name": "Neil",
		"last_name": "Powers",
        "address": "123 45th Ave",
		"phone_number": "4085555555",
		"id": 2
	},
	"price": 22000.0,
	"id": 16
}
```

return value of list sales:

```json
{
	"sales": [
    {
        "automobile": {
            "import_href": "/api/automobiles/1C3CC5FB2AN120174/",
            "vin": "1C3CC5FB2AN120174",
            "sold": true
        },
        "salesperson": {
            "first_name": "Tara",
            "last_name": "George",
            "employee_id": 7777,
            "id": 1
        },
        "customer": {
            "first_name": "Neil",
            "last_name": "Powers",
            "address": "123 45th Ave",
            "phone_number": "4085555555",
            "id": 2
        },
        "price": 22000.0,
        "id": 16
    }
    ]
}
```

return value of successful delete:

```json
{
	"message": "delete successful"
}
```

return value of trying to delete a nonexistent sale:

```json
{
	"error": "no such sale"
}
```

### AutomobileVO model

The AutobomileVO model gets data from the inventory microservice Automobile model using a poller. Automobile value objects have a "sold" attribute with a default value of False when they are created. The "sold" attribute is changed to True when an automobile is referenced in a new sale.
